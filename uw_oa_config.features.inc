<?php
/**
 * @file
 * uw_oa_config.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_oa_config_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_panels_mini_alter().
 */
function uw_oa_config_default_panels_mini_alter(&$data) {
  if (isset($data['oa_footer_panel'])) {
    unset($data['oa_footer_panel']->display->content['new-acef9208-6753-4fee-8b2b-834b2ed0909a']);
    unset($data['oa_footer_panel']->display->panels['footer'][1]);
  }
}
