<?php
/**
 * @file
 * uw_oa_config.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_oa_config_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'colorbox_style';
  $strongarm->value = 'sites/all/libraries/colorbox/example1';
  $export['colorbox_style'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'colorizer_oa_radix_palette';
  $strongarm->value = array(
    'top' => '#222222',
    'bottom' => '#111111',
    'toolbarbtn' => '#333333',
    'toolbarborder' => '#333333',
    'bg' => '#ffffff',
    'contentareabg' => '#ffffff',
    'headerbg' => '#000000',
    'menubg' => '#111111',
    'sidebar' => '#ffffff',
    'sidebartopbg' => '#ffffff',
    'sidebarbottombg' => '#e6e6e6',
    'sidebarborders' => '#e3e3e3',
    'sidebarheadertext' => '#3b3b3b',
    'sidebartext' => '#3b3b3b',
    'footer' => '#000000',
    'toolbartext' => '#fffeff',
    'menutext' => '#fffeff',
    'titleslogan' => '#fffeff',
    'text' => '#3b3b3b',
    'link' => '#0071B3',
    'linkactive' => '#23aeff',
    'linkhover' => '#018fe2',
  );
  $export['colorizer_oa_radix_palette'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'panels_page_allowed_layouts';
  $strongarm->value = 'O:22:"panels_allowed_layouts":4:{s:9:"allow_new";b:1;s:11:"module_name";s:11:"panels_page";s:23:"allowed_layout_settings";a:74:{s:8:"flexible";b:0;s:14:"twocol_stacked";b:0;s:13:"twocol_bricks";b:0;s:6:"twocol";b:0;s:25:"threecol_33_34_33_stacked";b:0;s:17:"threecol_33_34_33";b:0;s:25:"threecol_25_50_25_stacked";b:0;s:17:"threecol_25_50_25";b:0;s:6:"onecol";b:0;s:6:"whelan";b:0;s:12:"webb_flipped";b:0;s:5:"sutro";b:0;s:13:"selby_flipped";b:0;s:5:"selby";b:0;s:4:"pond";b:0;s:15:"moscone_flipped";b:0;s:7:"moscone";b:0;s:8:"mccoppin";b:0;s:15:"hewston_flipped";b:0;s:7:"hewston";b:0;s:6:"harris";b:0;s:5:"geary";b:0;s:12:"burr_flipped";b:0;s:4:"burr";b:0;s:5:"brown";b:0;s:15:"brenham_flipped";b:0;s:7:"brenham";b:0;s:16:"bartlett_flipped";b:0;s:8:"bartlett";b:0;s:17:"sanderson_flipped";b:0;s:9:"sanderson";b:0;s:6:"boxton";b:0;s:22:"bryant_flipped_flipped";b:0;s:6:"bryant";b:0;s:6:"phelan";b:0;s:14:"taylor_flipped";b:0;s:6:"taylor";b:0;s:12:"sutro_double";b:0;s:5:"rolph";b:0;s:4:"webb";b:0;s:10:"responsive";b:1;s:18:"responsive:default";b:0;s:14:"bryant_flipped";b:0;s:14:"radix_bartlett";b:1;s:22:"radix_bartlett_flipped";b:1;s:12:"radix_boxton";b:1;s:13:"radix_brenham";b:1;s:21:"radix_brenham_flipped";b:1;s:11:"radix_brown";b:1;s:12:"radix_bryant";b:1;s:20:"radix_bryant_flipped";b:1;s:10:"radix_burr";b:1;s:18:"radix_burr_flipped";b:1;s:11:"radix_geary";b:1;s:12:"radix_harris";b:1;s:13:"radix_hewston";b:1;s:21:"radix_hewston_flipped";b:1;s:14:"radix_mccoppin";b:1;s:13:"radix_moscone";b:1;s:21:"radix_moscone_flipped";b:1;s:12:"radix_phelan";b:1;s:10:"radix_pond";b:1;s:11:"radix_rolph";b:1;s:15:"radix_sanderson";b:1;s:23:"radix_sanderson_flipped";b:1;s:11:"radix_selby";b:1;s:19:"radix_selby_flipped";b:1;s:11:"radix_sutro";b:1;s:18:"radix_sutro_double";b:1;s:12:"radix_taylor";b:1;s:20:"radix_taylor_flipped";b:1;s:10:"radix_webb";b:1;s:18:"radix_webb_flipped";b:1;s:12:"radix_whelan";b:1;}s:10:"form_state";N;}';
  $export['panels_page_allowed_layouts'] = $strongarm;

  return $export;
}
