<?php
/**
 * @file
 * uw_oa_config.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_oa_config_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: panels_mini
  $overrides["panels_mini.oa_footer_panel.display|content|new-acef9208-6753-4fee-8b2b-834b2ed0909a"]["DELETED"] = TRUE;
  $overrides["panels_mini.oa_footer_panel.display|panels|footer|1"]["DELETED"] = TRUE;

 return $overrides;
}
