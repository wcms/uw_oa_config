<?php
/**
 * @file
 * Template for Default Home page panel.
 */
?>
<div class="big-wrapper">
  <div class="hero">
    <div class="hero-left-graphic"></div><!--/hero-right-graphic-->
    <div class="hero-right-graphic"></div><!--/hero-right-graphic-->
    <h1></h1>
    <h2>Welcome <?php if (!empty($username)) {print '<span>' . $username . '</span>'; } ?> to Collaborate!</h2>
    <?php if (!empty($login)): ?>
      <h3></h3>
    <?php else: ?>
      <h3><a href="<?php print url('sitemap'); ?>" class="btn btn-lg btn-success">Create Content</a></h3>
    <?php endif; ?>
  </div><!--/hero-->

  <div class="modules">

  <?php if (!empty($login)): ?>
    <div class="user-login">
      <?php print render($login); ?>
    </div>
  <?php endif; ?>

  </div><!--/modules-->

</div><!--/big wrapper-->